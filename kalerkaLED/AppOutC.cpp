// AppOutC.cpp: implementation of the CAppOutC class.
//
//////////////////////////////////////////////////////////////////////
#ifdef WIN32
#include "stdafx.h"
#else
#include <iostream>
#include <cstring>
#endif

#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "AppOutC.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAppOutC::CAppOutC() {

}

CAppOutC::~CAppOutC() {

}

//////////////////////////////////////////////////////////////////////////
// 名称：GetWaterSize
// 功能：获取流水边框bmp文件转换成控制卡需要的空间大小
// 参数：char* bmpfilename	-(read) bmp文件指针，包含完整的路径
//		 UINT8 basecolor	-(read) 控制卡的基色数目
//		 UINT32* picsizeH	-(write) 空间大小(字节数)
//		 UINT32* picsizeV	-(write) 空间大小(字节数)
// 返回：GETPICTURE_RESULT	-获取结果
// 创建：2015-02-15
// 修改：
//////////////////////////////////////////////////////////////////////////
GETPICTURE_RESULT CAppOutC::GetWaterSize(char *bmpfilename/*包含路径*/,
		UINT8 basecolor, UINT32* picsizeH, UINT32* picsizeV) {
	FILE *fp;
	myBITMAPFILEHEADER mybitmapfileheader;
	myBITMAPINFOHEADER mybitmapinfoheader;
	fp = fopen(bmpfilename, "r");

	if (fp == NULL) {
		return GETPICTURE_NOFILE;
	}

	fread(&mybitmapfileheader, 1, 14/*sizeof(myBITMAPFILEHEADER)*/, fp);
	fread(&mybitmapinfoheader, 1, sizeof(myBITMAPINFOHEADER), fp);

	if ((0 != memcmp((char*) &mybitmapfileheader.bfType, "BM", 2))
			&& (0 != memcmp((char*) &mybitmapfileheader.bfType, "MB", 2))) {
		return GETPICTURE_NOTBMP;
	}

	if ((mybitmapinfoheader.biBitCount != 32)
			&& (mybitmapinfoheader.biBitCount != 24)) {
		return GETPICTURE_NOT24OR32BITS;
	}

	fclose(fp);

	*picsizeH = (mybitmapinfoheader.biWidth + 15) / 16
			* mybitmapinfoheader.biHeight * basecolor * sizeof(UINT16) * 2;
	*picsizeV = (mybitmapinfoheader.biHeight + 15) / 16
			* mybitmapinfoheader.biWidth * basecolor * sizeof(UINT16) * 2;

	return GETPICTURE_SUCCESS;
}

//////////////////////////////////////////////////////////////////////////
// 名称：GetPictureSize
// 功能：获取bmp文件转换成控制卡需要的空间大小
// 参数：char* bmpfilename	-(read) bmp文件指针，包含完整的路径
//		 UINT8 basecolor	-(read) 控制卡的基色数目
//		 UINT32* picsize	-(write) 空间大小(字节数)
// 返回：GETPICTURE_RESULT	-获取结果
// 创建：2015-02-15
// 修改：
//////////////////////////////////////////////////////////////////////////
GETPICTURE_RESULT CAppOutC::GetPictureSize(char *bmpfilename/*包含路径*/,
		UINT8 basecolor, UINT32* picsize) {
	FILE *fp;
	myBITMAPFILEHEADER mybitmapfileheader;
	myBITMAPINFOHEADER mybitmapinfoheader;

	fp = fopen(bmpfilename, "r");

	if (fp == NULL) {
		return GETPICTURE_NOFILE;
	}

	fread(&mybitmapfileheader, 1, 14/*sizeof(myBITMAPFILEHEADER)*/, fp);
	fread(&mybitmapinfoheader, 1, sizeof(myBITMAPINFOHEADER), fp);

	if ((0 != memcmp((char*) &mybitmapfileheader.bfType, "BM", 2))
			&& (0 != memcmp((char*) &mybitmapfileheader.bfType, "MB", 2))) {
		return GETPICTURE_NOTBMP;
	}

	if ((mybitmapinfoheader.biBitCount != 32)
			&& (mybitmapinfoheader.biBitCount != 24)) {
		return GETPICTURE_NOT24OR32BITS;
	}

	fclose(fp);

	*picsize = (mybitmapinfoheader.biWidth + 15) / 16
			* mybitmapinfoheader.biHeight * basecolor * sizeof(UINT16);
	return GETPICTURE_SUCCESS;
}

//////////////////////////////////////////////////////////////////////////
// 名称：ConvertPictureContent
// 功能：获取bmp文件转换成控制卡数据
// 参数：char* bmpfilename	-(read) bmp文件指针，包含完整的路径
//		 UINT8 basecolor	-(read) 控制卡的基色数目
//		 UINT32* bmpcontent	-(write) 生成控制卡数据指针
// 返回：GETPICTURE_RESULT	-转换结果
// 创建：2015-02-15
// 修改：
//////////////////////////////////////////////////////////////////////////
GETPICTURE_RESULT CAppOutC::ConvertPictureContent(char *bmpfilename/*包含路径*/,
		UINT8 basecolor, UINT16* bmpcontent, UINT8 bigendian) {
	//mylog("ConvertPictureContent Start");
	FILE *fp;
	myBITMAPFILEHEADER mybitmapfileheader;
	myBITMAPINFOHEADER mybitmapinfoheader;
	INT16 bmpdatawidth; //bmp文件中组织的实际宽度
	INT32 i, j, k;
	myRGB32 RGB32;
	myRGB24 RGB24;
	UINT16 rgbdata[3];
	UINT8 rgbdatatmp;

	fp = fopen(bmpfilename, "r");
	mylog(bmpfilename);

	if (fp == NULL) {
		return GETPICTURE_NOFILE;
	}

	fread(&mybitmapfileheader, 14, 1, fp);
	fread(&mybitmapinfoheader, sizeof(myBITMAPINFOHEADER), 1, fp);

	if ((0 != memcmp((char*) &mybitmapfileheader.bfType, "BM", 2))
			&& (0 != memcmp((char*) &mybitmapfileheader.bfType, "MB", 2))) {
		return GETPICTURE_NOTBMP;
	}

	bmpdatawidth = (mybitmapinfoheader.biWidth + 15) / 16;
//	mylog("mybitmapinfoheader.biWidth");
//	mylog1(mybitmapinfoheader.biWidth);
//	mylog1(mybitmapinfoheader.biHeight);
//	mylog1(mybitmapinfoheader.biBitCount);
	if (mybitmapinfoheader.biBitCount == 32) {
		for (i = 0; i < mybitmapinfoheader.biHeight; ++i) {
			//行上整数部分
			for (j = 0; j < mybitmapinfoheader.biWidth / 16; ++j) {
				for (k = 0; k < 3; ++k) {
					rgbdata[k] = 0;
				}

				for (k = 0; k < 16; ++k) {
					fread(&RGB32, sizeof(myRGB32), 1, fp);
					if (RGB32.rgbRed > 0x80) {
						rgbdata[0] |= (1ul << (15 - k));
					}
					if (RGB32.rgbGreen > 0x80) {
						rgbdata[1] |= (1ul << (15 - k));
					}
					if (RGB32.rgbBlue > 0x80) {
						rgbdata[2] |= (1ul << (15 - k));
					}
				}

				if (bigendian) {
					for (k = 0; k < 3; ++k) {
						rgbdatatmp = rgbdata[k] & 0xff;
						rgbdata[k] >>= 8;
						rgbdata[k] |= (rgbdatatmp << 8);
					}
				}

				for (k = 0; k < basecolor; ++k) {
					bmpcontent[bmpdatawidth * mybitmapinfoheader.biHeight * k //基色数据偏移
					+ bmpdatawidth * (mybitmapinfoheader.biHeight - 1 - i) //行偏移
					+ j] = rgbdata[k]; //字偏移
				}
			}

			//行上剩下不足16bit的数据
			if (0 != (mybitmapinfoheader.biWidth % 16)) {
				for (k = 0; k < 3; ++k) {
					rgbdata[k] = 0;
				}

				for (k = 0; k < mybitmapinfoheader.biWidth % 16; ++k) {
					fread(&RGB32, sizeof(myRGB32), 1, fp);

					if (RGB32.rgbRed > 0x80) {
						rgbdata[0] |= (1ul << (15 - k));
					}

					if (RGB32.rgbGreen > 0x80) {
						rgbdata[1] |= (1ul << (15 - k));
					}

					if (RGB32.rgbBlue > 0x80) {
						rgbdata[2] |= (1ul << (15 - k));
					}
				}

				if (bigendian) {
					for (k = 0; k < 3; ++k) {
						rgbdatatmp = rgbdata[k] & 0xff;
						rgbdata[k] >>= 8;
						rgbdata[k] |= (rgbdatatmp << 8);
					}
				}

				for (k = 0; k < basecolor; ++k) {
					bmpcontent[bmpdatawidth * mybitmapinfoheader.biHeight * k //基色数据偏移
					+ bmpdatawidth * (mybitmapinfoheader.biHeight - 1 - i) //行偏移
					+ j] = rgbdata[k]; //字偏移
				}
			}

			//32位色不存在4字节对齐的问题
		}
	} else if (mybitmapinfoheader.biBitCount == 24) {
		for (i = 0; i < mybitmapinfoheader.biHeight; ++i) {
			//行上整数部分
			for (j = 0; j < mybitmapinfoheader.biWidth / 16; ++j) {
				for (k = 0; k < 3; ++k) {
					rgbdata[k] = 0;
				}

				for (k = 0; k < 16; ++k) {
					fread(&RGB24, 3, 1, fp);
					if (RGB24.rgbRed > 0x80) {
						rgbdata[0] |= (1ul << (15 - k));
					}
					if (RGB24.rgbGreen > 0x80) {
						rgbdata[1] |= (1ul << (15 - k));
					}
					if (RGB24.rgbBlue > 0x80) {
						rgbdata[2] |= (1ul << (15 - k));
					}
				}

				if (bigendian) {
					for (k = 0; k < 3; ++k) {
						rgbdatatmp = rgbdata[k] & 0xff;
						rgbdata[k] >>= 8;
						rgbdata[k] |= (rgbdatatmp << 8);
					}
				}

				for (k = 0; k < basecolor; ++k) {
					bmpcontent[bmpdatawidth * mybitmapinfoheader.biHeight * k //基色数据偏移
					+ bmpdatawidth * (mybitmapinfoheader.biHeight - 1 - i) //行偏移
					+ j] = rgbdata[k]; //字偏移
				}
			}

			//行上剩下不足16bit的数据
			if (0 != (mybitmapinfoheader.biWidth % 16)) {
				for (k = 0; k < 3; ++k) {
					rgbdata[k] = 0;
				}

				for (k = 0; k < mybitmapinfoheader.biWidth % 16; ++k) {
					fread(&RGB24, 3, 1, fp);

					if (RGB24.rgbRed > 0x80) {
						rgbdata[0] |= (1ul << (15 - k));
					}

					if (RGB24.rgbGreen > 0x80) {
						rgbdata[1] |= (1ul << (15 - k));
					}

					if (RGB24.rgbBlue > 0x80) {
						rgbdata[2] |= (1ul << (15 - k));
					}
				}

				if (bigendian) {
					for (k = 0; k < 3; ++k) {
						rgbdatatmp = rgbdata[k] & 0xff;
						rgbdata[k] >>= 8;
						rgbdata[k] |= (rgbdatatmp << 8);
					}
				}

				for (k = 0; k < basecolor; ++k) {
					bmpcontent[bmpdatawidth * mybitmapinfoheader.biHeight * k //基色数据偏移
					+ bmpdatawidth * (mybitmapinfoheader.biHeight - 1 - i) //行偏移
					+ j] = rgbdata[k]; //字偏移
				}
			}

			//每行必须4字节对齐
			if (0 != (mybitmapinfoheader.biWidth * 3 % 4)) {
				fread(&RGB24, 4 - mybitmapinfoheader.biWidth * 3 % 4, 1, fp);
			}
		}
	} else {
		return GETPICTURE_NOT24OR32BITS;
	}

	fclose(fp);
	//mylog("end");
	return GETPICTURE_SUCCESS;
}

GETPICTURE_RESULT CAppOutC::ConvertPictureContentX(char *filecontent/*包含路径*/,
		UINT8 basecolor, UINT16* bmpcontent, UINT8 bigendian) {
	//mylog("ConvertPictureContent Start");
	//FILE *fp;
	myBITMAPFILEHEADER mybitmapfileheader;
	myBITMAPINFOHEADER mybitmapinfoheader;
	INT16 bmpdatawidth; //bmp文件中组织的实际宽度
	INT32 i, j, k;
	myRGB32 RGB32;
	myRGB24 RGB24;
	UINT16 rgbdata[3];
	UINT8 rgbdatatmp;
	char* pfilecontent;

	//fp = fopen(bmpfilename, "r");
	//mylog(bmpfilename);

	if (filecontent == NULL) {
		return GETPICTURE_NOFILE;
	}

	memcpy(&mybitmapfileheader, filecontent, 14);
	memcpy(&mybitmapinfoheader, filecontent + 14, sizeof(myBITMAPINFOHEADER));

	if ((0 != memcmp((char*) &mybitmapfileheader.bfType, "BM", 2))
			&& (0 != memcmp((char*) &mybitmapfileheader.bfType, "MB", 2))) {
		return GETPICTURE_NOTBMP;
	}

	pfilecontent = filecontent + 14 + sizeof(myBITMAPINFOHEADER);

	bmpdatawidth = (mybitmapinfoheader.biWidth + 15) / 16;
	//mylog("mybitmapinfoheader.biWidth");
	//mylog1(mybitmapinfoheader.biWidth);
	//mylog1(mybitmapinfoheader.biHeight);
	//mylog1(mybitmapinfoheader.biBitCount);
	if (mybitmapinfoheader.biBitCount == 32) {
		for (i = 0; i < mybitmapinfoheader.biHeight; ++i) {
			//行上整数部分
			for (j = 0; j < mybitmapinfoheader.biWidth / 16; ++j) {
				for (k = 0; k < 3; ++k) {
					rgbdata[k] = 0;
				}

				for (k = 0; k < 16; ++k) {
					memcpy(&RGB32, pfilecontent, sizeof(myRGB32));
					pfilecontent +=sizeof(myRGB32);
					if (RGB32.rgbRed > 0x80) {
						rgbdata[0] |= (1ul << (15 - k));
					}
					if (RGB32.rgbGreen > 0x80) {
						rgbdata[1] |= (1ul << (15 - k));
					}
					if (RGB32.rgbBlue > 0x80) {
						rgbdata[2] |= (1ul << (15 - k));
					}
				}

				if (bigendian) {
					for (k = 0; k < 3; ++k) {
						rgbdatatmp = rgbdata[k] & 0xff;
						rgbdata[k] >>= 8;
						rgbdata[k] |= (rgbdatatmp << 8);
					}
				}

				for (k = 0; k < basecolor; ++k) {
					bmpcontent[bmpdatawidth * mybitmapinfoheader.biHeight * k //基色数据偏移
					+ bmpdatawidth * (mybitmapinfoheader.biHeight - 1 - i) //行偏移
					+ j] = rgbdata[k]; //字偏移
				}
			}

			//行上剩下不足16bit的数据
			if (0 != (mybitmapinfoheader.biWidth % 16)) {
				for (k = 0; k < 3; ++k) {
					rgbdata[k] = 0;
				}

				for (k = 0; k < mybitmapinfoheader.biWidth % 16; ++k) {
					memcpy(&RGB32, pfilecontent, sizeof(myRGB32));
					pfilecontent +=sizeof(myRGB32);


					if (RGB32.rgbRed > 0x80) {
						rgbdata[0] |= (1ul << (15 - k));
					}

					if (RGB32.rgbGreen > 0x80) {
						rgbdata[1] |= (1ul << (15 - k));
					}

					if (RGB32.rgbBlue > 0x80) {
						rgbdata[2] |= (1ul << (15 - k));
					}
				}

				if (bigendian) {
					for (k = 0; k < 3; ++k) {
						rgbdatatmp = rgbdata[k] & 0xff;
						rgbdata[k] >>= 8;
						rgbdata[k] |= (rgbdatatmp << 8);
					}
				}

				for (k = 0; k < basecolor; ++k) {
					bmpcontent[bmpdatawidth * mybitmapinfoheader.biHeight * k //基色数据偏移
					+ bmpdatawidth * (mybitmapinfoheader.biHeight - 1 - i) //行偏移
					+ j] = rgbdata[k]; //字偏移
				}
			}

			//32位色不存在4字节对齐的问题
		}
	} else if (mybitmapinfoheader.biBitCount == 24) {
		for (i = 0; i < mybitmapinfoheader.biHeight; ++i) {
			//行上整数部分
			for (j = 0; j < mybitmapinfoheader.biWidth / 16; ++j) {
				for (k = 0; k < 3; ++k) {
					rgbdata[k] = 0;
				}

				for (k = 0; k < 16; ++k) {
					memcpy(&RGB24, pfilecontent, 3);
					pfilecontent +=3;

					if (RGB24.rgbRed > 0x80) {
						rgbdata[0] |= (1ul << (15 - k));
					}
					if (RGB24.rgbGreen > 0x80) {
						rgbdata[1] |= (1ul << (15 - k));
					}
					if (RGB24.rgbBlue > 0x80) {
						rgbdata[2] |= (1ul << (15 - k));
					}
				}

				if (bigendian) {
					for (k = 0; k < 3; ++k) {
						rgbdatatmp = rgbdata[k] & 0xff;
						rgbdata[k] >>= 8;
						rgbdata[k] |= (rgbdatatmp << 8);
					}
				}

				for (k = 0; k < basecolor; ++k) {
					bmpcontent[bmpdatawidth * mybitmapinfoheader.biHeight * k //基色数据偏移
					+ bmpdatawidth * (mybitmapinfoheader.biHeight - 1 - i) //行偏移
					+ j] = rgbdata[k]; //字偏移
				}
			}

			//行上剩下不足16bit的数据
			if (0 != (mybitmapinfoheader.biWidth % 16)) {
				for (k = 0; k < 3; ++k) {
					rgbdata[k] = 0;
				}

				for (k = 0; k < mybitmapinfoheader.biWidth % 16; ++k) {
					memcpy(&RGB24, pfilecontent, 3);
					pfilecontent +=3;


					if (RGB24.rgbRed > 0x80) {
						rgbdata[0] |= (1ul << (15 - k));
					}

					if (RGB24.rgbGreen > 0x80) {
						rgbdata[1] |= (1ul << (15 - k));
					}

					if (RGB24.rgbBlue > 0x80) {
						rgbdata[2] |= (1ul << (15 - k));
					}
				}

				if (bigendian) {
					for (k = 0; k < 3; ++k) {
						rgbdatatmp = rgbdata[k] & 0xff;
						rgbdata[k] >>= 8;
						rgbdata[k] |= (rgbdatatmp << 8);
					}
				}

				for (k = 0; k < basecolor; ++k) {
					bmpcontent[bmpdatawidth * mybitmapinfoheader.biHeight * k //基色数据偏移
					+ bmpdatawidth * (mybitmapinfoheader.biHeight - 1 - i) //行偏移
					+ j] = rgbdata[k]; //字偏移
				}
			}

			//每行必须4字节对齐
			if (0 != (mybitmapinfoheader.biWidth * 3 % 4)) {
				pfilecontent +=4 - mybitmapinfoheader.biWidth * 3 % 4;
			}
		}
	} else {
		return GETPICTURE_NOT24OR32BITS;
	}

	//fclose(fp);
	//mylog("end");
	return GETPICTURE_SUCCESS;
}

//////////////////////////////////////////////////////////////////////////
// 名称：GetScfSef
// 功能：获取Sef文件
// 参数：char* screenname	-(read) 屏幕的名称 应该包xscan的路径
//		 char* scfbuffer	-(write) 生成的scf文件指针
//		 char* sefbuffer	-(write) 生成的sef文件指针
// 返回：bool	-转换结果
// 创建：2015-02-15
// 修改：
//////////////////////////////////////////////////////////////////////////
bool CAppOutC::GetScfSef(char *screenname, char *scfbuffer, char *sefbuffer) {
	FILE *fp;
	char* bmpfilename;

	if ((screenname == NULL) || (scfbuffer == NULL) || (sefbuffer == NULL)) {
		return false;
	}

	//TODO
	bmpfilename = (char*) malloc(strlen(screenname) + 10);
	sprintf(bmpfilename, "%s.scf", screenname);

	fp = fopen(bmpfilename, "r");
	if (fp == NULL) {
		free(bmpfilename);
		return false;
	}
	fread(scfbuffer, sizeof(LEDFILEDCB), 1, fp);
	fclose(fp);

	sprintf(bmpfilename, "%s.sef", screenname);
	fp = fopen(bmpfilename, "r");
	if (fp == NULL) {
		free(bmpfilename);
		return false;
	}
	fread(sefbuffer, sizeof(LEDFILEBLK), 1, fp);
	fclose(fp);

	free(bmpfilename);

	return true;
}

//////////////////////////////////////////////////////////////////////////
// 名称：CreateScfSef
// 功能：根据结构体参数生成Sef文件
// 参数：LEDPARADCB *ledparadcb		-(read) 屏幕参数结构体
//		 LEDPARABLK *ledparablk		-(read) 屏幕扩展参数结构体
//		 char* scfbuffer			-(write) 生成的scf文件指针  必须是4字节对齐的指针
//		 char* sefbuffer			-(write) 生成的sef文件指针  必须是4字节对齐的指针
// 返回：bool	-转换结果
// 创建：2015-02-15
// 修改：
//////////////////////////////////////////////////////////////////////////
bool CAppOutC::CreateScfSef(LEDPARADCB *ledparadcb, LEDPARABLK *ledparablk,
		char *scfbuffer, char *sefbuffer) {
	//  /*LEDPARADCB结构体*************************************
	// 	ledparadcb->name[16];			// 正确填写
	// 	ledparadcb->width;				// 正确填写
	// 	ledparadcb->height;				// 正确填写
	// 	ledparadcb->linesperinterface;	// 正确填写
	// 	ledparadcb->lines;				// 正确填写
	// 	ledparadcb->wrapbits;			// 正确填写
	// 	ledparadcb->lineverify;			// 正确填写
	// 	ledparadcb->gray_basecolor;		// 正确填写
	// 	ledparadcb->frequency;			// 正确填写
	// 	ledparadcb->lineblanking;		// 正确填写
	// 	ledparadcb->linemode;			// 正确填写
	// 	ledparadcb->bmask;				// 正确填写
	// 	ledparadcb->typeindex;			// 正确填写
	// 
	// 	ledparadcb->d_ipaddr[6];		// 填写成0
	// 	ledparadcb->d_submask[6];		// 填写成0
	// 	ledparadcb->d_gateway[6];		// 填写成0
	// 	ledparadcb->d_port;				// 填写成0
	// 	ledparadcb->d_serveripaddr1[6];	// 填写成0
	// 	ledparadcb->d_serverport1;		// 填写成0
	// 	ledparadcb->d_serveripaddr2[6];	// 填写成0
	// 	ledparadcb->d_serverport2;		// 填写成0
	// 	ledparadcb->reserve;			// 填写成0
	//  **************************************************************/

	//  /*LEDPARABLK结构体*************************************
	// 	ledparablk->reserve;
	// 	ledparablk->dotindex[DOTINDEX_MAX];	// 正确填写
	// 	ledparablk->lineverify[16];			// 正确填写
	// 	ledparablk->bmask;					// 正确填写  注意和LEDPARADCB的相同意义的bit位的值保持一致
	// 	ledparablk->typeindex;				// 正确填写  这个值传送给这个参数的时候一定要和LEDPARADCB的参数一致
	// 	ledparablk->linesperinterface;		// 正确填写  注意和LEDPARADCB的值保持一致
	// 	ledparablk->Ztimes;					// 正确填写
	// 	ledparablk->gama;					// 正确填写
	// 	ledparablk->module_width;			// 正确填写
	// 	ledparablk->module_empty;			// 正确填写
	//	**************************************************************/

	LEDFILEDCB* ledfiledcb;
	LEDFILEBLK* ledfileblk;

	if ((ledparadcb == NULL) || (ledparablk == NULL) || (scfbuffer == NULL)
			|| (sefbuffer == NULL)) {
		return false;
	}

	ledfiledcb = (LEDFILEDCB*) scfbuffer;
	ledfileblk = (LEDFILEBLK*) sefbuffer;

	ledfiledcb->fileheaderdcb.signature[0] = 'S';
	ledfiledcb->fileheaderdcb.signature[1] = 'C';
	ledfiledcb->fileheaderdcb.signature[2] = 'F';
	ledfiledcb->fileheaderdcb.version = 1;
	ledfiledcb->fileheaderdcb.length = sizeof(LEDFILEDCB);
	memcpy(&ledfiledcb->ledparadcb, ledparadcb, sizeof(LEDPARADCB));

	ledfileblk->fileheaderdcb.signature[0] = 'S';
	ledfileblk->fileheaderdcb.signature[1] = 'E';
	ledfileblk->fileheaderdcb.signature[2] = 'F';
	ledfileblk->fileheaderdcb.version = 1;
	ledfileblk->fileheaderdcb.length = sizeof(LEDFILEBLK);
	memcpy(&ledfileblk->ledparablk, ledparablk, sizeof(LEDPARABLK));

	return true;
}

//////////////////////////////////////////////////////////////////////////
// 名称：CreateAcfPicture
// 功能：生成ACF picture文件
// 参数：PICTUREPARADCB* pictureparadcb		-(read) 图片区域参数
//		 SHOWEXPARADCB* showexparadcb		-(read) 图片扩展参数指针，是所有图片扩展参数组合
//		 SHOWPARADCB* showparadcb			-(read) 图片参数指针，是所有图片参数组合
//		 UINT32* piccontent					-(write) 图片内容位置指针
//		 UINT32* watercontent				-(write) 流水边框内容位置指针
//		 char* acfbuffer					-(write) acf文件指针
//		 UINT8 basecolor					-(read) 控制卡的基色数目
// 返回：bool	-转换结果
// 创建：2015-02-15
// 修改：
//////////////////////////////////////////////////////////////////////////
UINT32 CAppOutC::GetAcfPictureLength(PICTUREPARADCB* pictureparadcb,
		UINT8 basecolor) {
	UINT32 watersize;
	UINT32 picsize;
	UINT16 picwidth, picheight;

	picwidth = pictureparadcb->rectangularparadcb.width;
	picheight = pictureparadcb->rectangularparadcb.height;
//	mylog("picwidth");
//	mylog1(picwidth);
//	mylog("picheight");
//	mylog1(picheight);
	if (pictureparadcb->waterdisplaydcb.mode & 0x80) {	//有流水边框
		picwidth -= pictureparadcb->waterdisplaydcb.width;
		picheight -= pictureparadcb->waterdisplaydcb.height;
	}
	picsize = (picwidth + 15) / 16 * picheight * basecolor * sizeof(UINT16);
	watersize = (pictureparadcb->waterdisplaydcb.width + 15) / 16
			* pictureparadcb->waterdisplaydcb.height * 2 * basecolor
			* sizeof(UINT16)
			+ (pictureparadcb->waterdisplaydcb.height + 15) / 16
					* pictureparadcb->waterdisplaydcb.width * 2 * basecolor
					* sizeof(UINT16);

	return (sizeof(PICTUREFILEDCB)
			+ pictureparadcb->pictotol
					* (picsize + sizeof(SHOWPARADCB) + sizeof(SHOWEXPARADCB))
			+ watersize);
}

//////////////////////////////////////////////////////////////////////////
// 名称：CreateAcfPicture
// 功能：生成ACF picture文件
// 参数：PICTUREPARADCB* pictureparadcb		-(read) 图片区域参数
//		 SHOWEXPARADCB* showexparadcb		-(read) 图片扩展参数指针，是所有图片扩展参数组合
//		 SHOWPARADCB* showparadcb			-(read) 图片参数指针，是所有图片参数组合
//		 UINT32* piccontent					-(write) 图片内容位置指针
//		 UINT32* watercontent				-(write) 流水边框内容位置指针
//		 char* acfbuffer					-(write) acf文件指针   必须是4字节对齐的指针
//		 UINT8 basecolor					-(read) 控制卡的基色数目
// 返回：bool	-转换结果
// 创建：2015-02-15
// 修改：
//////////////////////////////////////////////////////////////////////////
bool CAppOutC::CreateAcfPicture(PICTUREPARADCB* pictureparadcb,
		SHOWEXPARADCB* showexparadcb, SHOWPARADCB* showparadcb,
		UINT32* piccontent, UINT32* watercontent, char* acfbuffer,
		UINT8 basecolor) {
	INT32 i;
	PICTUREFILEDCB* picturefiledcb;
	UINT32 watersize;
	UINT32 picsize;
	UINT16 picwidth, picheight;

	//mylog("22222222");
	if (acfbuffer == NULL) {

		//mylog("555555");
		return false;
	}

	//mylog("AAAAAA");
	//mylog("pictureparadcb22:");
	//mylog1(pictureparadcb->pictotol);
	picwidth = pictureparadcb->rectangularparadcb.width;
	//mylog("BBBBBB");
	picheight = pictureparadcb->rectangularparadcb.height;
	if (pictureparadcb->waterdisplaydcb.mode & 0x80) {	//有流水边框
		picwidth -= pictureparadcb->waterdisplaydcb.width;
		picheight -= pictureparadcb->waterdisplaydcb.height;
	}
	//mylog("3333");
	picsize = (picwidth + 15) / 16 * picheight * basecolor * sizeof(UINT16);
	watersize = (pictureparadcb->waterdisplaydcb.width + 15) / 16
			* pictureparadcb->waterdisplaydcb.height * 2 * basecolor
			* sizeof(UINT16)
			+ (pictureparadcb->waterdisplaydcb.height + 15) / 16
					* pictureparadcb->waterdisplaydcb.width * 2 * basecolor
					* sizeof(UINT16);

	//mylog("4444");
	picturefiledcb = (PICTUREFILEDCB*) acfbuffer;

	picturefiledcb->fileheaderdcb.signature[0] = 'A';
	picturefiledcb->fileheaderdcb.signature[1] = 'C';
	picturefiledcb->fileheaderdcb.signature[2] = 'F';
	picturefiledcb->fileheaderdcb.version = 1;
	picturefiledcb->fileheaderdcb.length = sizeof(PICTUREFILEDCB)
			+ pictureparadcb->pictotol
					* (picsize + sizeof(SHOWPARADCB) + sizeof(SHOWEXPARADCB))
			+ watersize;


	//mylog("dddd");
	memcpy(&picturefiledcb->pictureparadcb, pictureparadcb,
			sizeof(PICTUREPARADCB));

	//mylog("pictureparadcb:");
	//mylog1(pictureparadcb->pictotol);

	//mylog("picturefiledcb->pictureparadcb.pictotol:");
	//mylog1(picturefiledcb->pictureparadcb.pictotol);
	picturefiledcb->pictureparadcb.reserve1[0] = 0;
	picturefiledcb->pictureparadcb.reserve1[1] = 0;
	picturefiledcb->pictureparadcb.reserve2 = 0;
	picturefiledcb->pictureparadcb.areatype = AREA_TYPE_PICTURE;
	picturefiledcb->pictureparadcb.icoparaaddr = 0;
	picturefiledcb->pictureparadcb.icototol = 0;
	picturefiledcb->pictureparadcb.picparaaddr = sizeof(PICTUREFILEDCB);
	picturefiledcb->pictureparadcb.waterdisplaydcb.watercontentoffset =
			sizeof(PICTUREFILEDCB)
					+ pictureparadcb->pictotol
							* (picsize + sizeof(SHOWPARADCB)
									+ sizeof(SHOWPARADCB));

	//mylog("eeee");
	//mylog("picturefiledcb->pictureparadcb.pictotol:");
	//mylog1(picturefiledcb->pictureparadcb.pictotol);
	for (i = 0; i < picturefiledcb->pictureparadcb.pictotol; ++i) {
		//mylog("kkkkk");
		//mylog1(i + 30);
		showparadcb[i].contentoffset = sizeof(PICTUREFILEDCB)
				+ picturefiledcb->pictureparadcb.pictotol * sizeof(SHOWPARADCB)
				+ i * (picsize + sizeof(SHOWEXPARADCB));
		//填充图片参数  图片参数在调用函数前已经组织好数据
		memcpy(acfbuffer + sizeof(PICTUREFILEDCB) + i * sizeof(SHOWPARADCB),
				showparadcb + i, sizeof(SHOWPARADCB));

		//mylog("eeee11111");
		//mylog("showparadcb[i].contentoffset:");
		//mylog1(showparadcb[i].contentoffset);
// 		//填充图片内容  图片内容在调用函数前已经组织好数据
// 		memcpy(acfbuffer +showparadcb[i].contentoffset,//sizeof(PICTUREFILEDCB) +picturefiledcb->pictureparadcb.pictotol*sizeof(SHOWPARADCB) +i*(picsize +sizeof(SHOWEXPARADCB)),
// 			piccontent +i*picsize, picsize);

		// delete? by daniel start
		piccontent[i] = (uint64_t) (acfbuffer + showparadcb[i].contentoffset);
		// delete? by daniel end
		//mylog("eeee2222");

		//填充图片扩展参数  图片内容在调用函数前已经组织好数据
		memcpy(
				acfbuffer + sizeof(PICTUREFILEDCB)
						+ picturefiledcb->pictureparadcb.pictotol
								* sizeof(SHOWPARADCB)
						+ i * (picsize + sizeof(SHOWEXPARADCB)) + picsize,
				showexparadcb + i, sizeof(SHOWEXPARADCB));
	}

	//mylog("ffff");

	//填充流水边框内容
	// delete? by daniel start
	*watercontent =
			(uint64_t) (acfbuffer
					+ picturefiledcb->pictureparadcb.waterdisplaydcb.watercontentoffset);
	// delete? by daniel end

	//mylog("gggg");

	return true;
}

//////////////////////////////////////////////////////////////////////////
// 名称：GetPcfLength
// 功能：获取PCF文件长度
// 参数：UINT16 areanum						-(read) 区域数量
//		 WATERDISPLAYDCB* waterdisplaydcb	-(read) 区域流水边框参数
//		 UINT8 basecolor					-(read) 控制卡的基色数目
// 返回：UINT32	-文件长度
// 创建：2015-02-15
// 修改：
//////////////////////////////////////////////////////////////////////////
UINT32 CAppOutC::GetPcfLength(UINT16 areanum, WATERDISPLAYDCB* waterdisplaydcb,
		UINT8 basecolor) {
	return (sizeof(PROGRAMFILEDCB) + areanum * 16
			+ (waterdisplaydcb->width + 15) / 16 * waterdisplaydcb->height * 2
					* basecolor * sizeof(UINT16)
			+ (waterdisplaydcb->height + 15) / 16 * waterdisplaydcb->width * 2
					* basecolor * sizeof(UINT16));
}

//////////////////////////////////////////////////////////////////////////
// 名称：CreatePcf
// 功能：生成pcf文件
// 参数：UINT16 areanum						-(read) 区域数量
//		 char* areafilename					-(read) 区域文件名 可能是N个，每16个是一个文件名
//		 WATERDISPLAYDCB* waterdisplaydcb	-(read) 区域流水边框参数
//		 UINT16* watercontent				-(write) 流水边框位置指针
//		 char* pcfbuffer					-(write) pcf文件的内容
//		 UINT8 basecolor					-(read) 控制卡的基色数目
// 返回：bool	-转换结果
// 创建：2015-02-15
// 修改：
//////////////////////////////////////////////////////////////////////////
bool CAppOutC::CreatePcf(UINT16 areanum, char* areafilename,
		WATERDISPLAYDCB* waterdisplaydcb, UINT32* watercontent, char* pcfbuffer,
		UINT8 basecolor) {
	//mylog("111");
	INT32 i;
	PROGRAMFILEDCB* programfiledcb;

	if (pcfbuffer == NULL) {
		return false;
	}

	programfiledcb = (PROGRAMFILEDCB*) pcfbuffer;

	programfiledcb->fileheaderdcb.signature[0] = 'P';
	programfiledcb->fileheaderdcb.signature[1] = 'C';
	programfiledcb->fileheaderdcb.signature[2] = 'F';
	programfiledcb->fileheaderdcb.version = 1;
	programfiledcb->fileheaderdcb.length = sizeof(PROGRAMFILEDCB) + areanum * 16/*16���ֽڵ��ļ�ÁE*/
			+ (waterdisplaydcb->width + 15) / 16 * waterdisplaydcb->height * 2
					* basecolor * sizeof(UINT16)
			+ (waterdisplaydcb->height + 15) / 16 * waterdisplaydcb->width * 2
					* basecolor * sizeof(UINT16);

	programfiledcb->reserve[0] = 0;
	programfiledcb->reserve[1] = 0;
	programfiledcb->areanum = areanum;
	if (waterdisplaydcb != NULL) {
		//TODO
		memcpy(&programfiledcb->waterdisplaydcb, &waterdisplaydcb,
				sizeof(WATERDISPLAYDCB));
		//UINT32 watercontentoffset;	//流水边框图片在此文件中相对此位置地址偏移量
		//INT8 width;					//一幅图片的上边和下边的宽度(即左边和右边的高度)最好以16的整数倍为一个循环单位且至少为height的偶数倍
		//INT8 height;				//一幅图片的上边和下边的高度，左边和有点的宽度
		//UINT8 speed;				//移动速度
		//UINT8 mode;
//		mylog("waterdisplaydcb->watercontentoffset");
//		mylog1(waterdisplaydcb->watercontentoffset);
//		mylog("programfiledcb->waterdisplaydcb->watercontentoffset");
//		mylog1((programfiledcb->waterdisplaydcb).watercontentoffset);
//		mylog("waterdisplaydcb->height");
//		mylog1(waterdisplaydcb->height);
//		mylog("programfiledcb->waterdisplaydcb->height");
//		mylog1((programfiledcb->waterdisplaydcb).height);
	} else {
		programfiledcb->waterdisplaydcb.height = 0;
		programfiledcb->waterdisplaydcb.width = 0;
		programfiledcb->waterdisplaydcb.mode = 0;
		programfiledcb->waterdisplaydcb.speed = 0;
	}
	//mylog("222");
	if (areafilename != NULL) {
		for (i = 0; i < areanum; ++i) {
			areafilename[i * 16 + 15] = '\0';		//保证文件名有结束 文件名的有效字节数为15
			memcpy(pcfbuffer + sizeof(PROGRAMFILEDCB) + i * 16,
					areafilename + i * 16, 16);
		}
	}

	programfiledcb->waterdisplaydcb.watercontentoffset = sizeof(PROGRAMFILEDCB)
			+ areanum * 16;
	//最后添加流水边框的内容
// 	if(watercontent !=NULL)
// 	{
// 		memcpy(pcfbuffer +sizeof(CONTENTFILEDCB) +areanum*16 +2/*保留字节*/ +sizeof(UINT16)/*区域数量*/,
// 			watercontent,
// 			(waterdisplaydcb->width +15)/16*waterdisplaydcb->height*2*basecolor*sizeof(UINT16)
// 			+(waterdisplaydcb->height +15)/16*waterdisplaydcb->width*2*basecolor*sizeof(UINT16));
// 	}
//#ifdef WIN32
	*watercontent =(uint64_t)(pcfbuffer +sizeof(PROGRAMFILEDCB) +areanum*16);
//	mylog1((UINT16)((UINT32)pcfbuffer>>16));
//	mylog1((UINT16)((UINT32)pcfbuffer>>0));
//
//	mylog1((UINT16)((UINT32)*watercontent>>16));
//	mylog1((UINT16)((UINT32)*watercontent>>0));
	// #else
	// delete? by daniel start
	//*watercontent =(UINT32)(pcfbuffer +sizeof(PROGRAMFILEDCB) +areanum*16);
	// delete? by daniel end	
//#endif

	return true;
}

//////////////////////////////////////////////////////////////////////////
// 名称：GetCcfLength
// 功能：获取CCF文件长度
// 参数：short programnum					-(read) 节目数量
//		 PROGRAMPARADCB* programparadcb		-(read) 节目参数
// 返回：UINT32	-CCF文件长度
// 创建：2015-02-15
// 修改：
//////////////////////////////////////////////////////////////////////////
UINT32 CAppOutC::GetCcfLength(short programnum) {
	return (sizeof(CONTENTFILEDCB) + programnum * sizeof(PROGRAMPARADCB));
}

//////////////////////////////////////////////////////////////////////////
// 名称：CreateCcf
// 功能：生成CCF文件
// 参数：short programnum					-(read) 节目数量
//		 PROGRAMPARADCB* programparadcb		-(read) 节目参数
//		 char* ccfbuffer					-(write) ccf文件的内容
// 返回：bool	-转换结果
// 创建：2015-02-15
// 修改：
//////////////////////////////////////////////////////////////////////////
bool CAppOutC::CreateCcf(short programnum, PROGRAMPARADCB* programparadcb,
		char* ccfbuffer) {
	INT32 i;
	CONTENTFILEDCB* contentfiledcb;

	if ((programnum < 0) || (programparadcb == NULL) || (ccfbuffer == NULL)) {
		return false;
	}

	contentfiledcb = (CONTENTFILEDCB*) ccfbuffer;

	contentfiledcb->fileheaderdcb.signature[0] = 'C';
	contentfiledcb->fileheaderdcb.signature[1] = 'C';
	contentfiledcb->fileheaderdcb.signature[2] = 'F';
	contentfiledcb->fileheaderdcb.version = 1;
	contentfiledcb->fileheaderdcb.length = sizeof(CONTENTFILEDCB)
			+ programnum * sizeof(PROGRAMPARADCB);

	contentfiledcb->contentparadcb.reserve[0] = 0;
	contentfiledcb->contentparadcb.reserve[1] = 0;
	contentfiledcb->contentparadcb.programnum = programnum;

	for (i = 0; i < programnum; ++i) {
		/*节目的参数应该在外部被赋值过*/
		programparadcb[i].backgroudfilename[15] = '\0';	//保证文件名有结束 文件名的有效字节数为15
		programparadcb[i].programfilename[15] = '\0';	//保证文件名有结束 文件名的有效字节数为15
		memcpy(ccfbuffer + sizeof(CONTENTFILEDCB) + i * sizeof(PROGRAMPARADCB),
				&programparadcb[i], sizeof(PROGRAMPARADCB));
	}

	return true;
}

UINT16 CAppOutC::GetFrameData(UINT16 frametype, char* filename,
		char* sendcontent, UINT16 sendlength, UINT32 sendindex,
		char* framebuffer) {
	UINT16 i;
	UINT8 check = 0;
	UINT16 framelength = 0;
	UINT16 actuallength = sendlength + 20;//20150330

	for (i = 0; i < RS232_RECEIVE_HEAD_SUM; ++i) {
		framebuffer[framelength++] = RS232_HEAD[i];
		check += RS232_HEAD[i];
	}

	for (i = 0; i < RS232_RECEIVE_DESADDR_SUM - 1; ++i) {
		framebuffer[framelength++] = '0';
		check += '0';
	}
	framebuffer[framelength++] = '1';
	check += '1';

	for (i = 0; i < RS232_RECEIVE_SRCADDR_SUM; ++i) {
		framebuffer[framelength++] = '0';
		check += '0';
	}

	//mylog("filenamefilenamefilenamefilenamefilename");
	//mylog(filename);
	framebuffer[framelength++] = (frametype / 1000 % 10) + '0';
	check += (frametype / 1000 % 10) + '0';
	framebuffer[framelength++] = frametype % 1000 / 100 + '0';
	check += frametype % 1000 / 100 + '0';
	framebuffer[framelength++] = frametype % 100 / 10 + '0';
	check += frametype % 100 / 10 + '0';
	framebuffer[framelength++] = frametype % 10 + '0';
	check += frametype % 10 + '0';
	//mylog("22222");
	framebuffer[framelength++] = (actuallength / 1000 % 10) + '0';
	check += (actuallength / 1000 % 10) + '0';
	framebuffer[framelength++] = actuallength % 1000 / 100 + '0';
	check += actuallength % 1000 / 100 + '0';
	framebuffer[framelength++] = actuallength % 100 / 10 + '0';
	check += actuallength % 100 / 10 + '0';
	framebuffer[framelength++] = actuallength % 10 + '0';
	check += actuallength % 10 + '0';
	//mylog("3333");

	/*文件名固定是16个字节*/
	for (i = 0; i < 16; ++i) {
		framebuffer[framelength++] = filename[i];
		check += filename[i];
	}
	//mylog("filenamefilenamefilenamefilenamef0000000000000000");
	//mylog(framebuffer + framelength -16);

	framebuffer[framelength++] = sendindex >> 0;
	check += sendindex >> 0;
	framebuffer[framelength++] = sendindex >> 8;
	check += sendindex >> 8;
	framebuffer[framelength++] = sendindex >> 16;
	check += sendindex >> 16;
	framebuffer[framelength++] = sendindex >> 24;
	check += sendindex >> 24;

	for (i = 0; i < sendlength; ++i) {
		framebuffer[framelength++] = sendcontent[i+sendindex];
		check += sendcontent[i+sendindex];
	}

	for (i = 0; i < RS232_RECEIVE_TRAIL_SUM; ++i) {
		framebuffer[framelength++] = RS232_TRAIL[i];
		check += RS232_TRAIL[i];
	}

	sprintf(framebuffer + framelength, "%02X", check);
	framelength += 2;

	return framelength;
}

UINT16 CAppOutC::GetScfFrameData(char* sendcontent,	//文件内容指针
		char* framebuffer)	//生成的帧内容指针 在外部malloc一块区域，注意这块区域的大小
		{
	return GetFrameData(PARAFILE_SET, "all.scf", sendcontent,
			sizeof(LEDFILEDCB), 0, framebuffer);
}

UINT16 CAppOutC::GetSefFrameData(char* sendcontent,	//文件内容指针
		char* framebuffer)	//生成的帧内容指针 在外部malloc一块区域，注意这块区域的大小
		{
	return GetFrameData(PARAEXTFILE_SET, "all.sef", sendcontent,
			sizeof(LEDFILEBLK), 0, framebuffer);
}

UINT16 CAppOutC::GetContentFrameData(char* filename,//ccf pcf和acf文件的真是文件名 一个工程里面不能有相同的文件名，需要软件做内容的格式命名
		char* sendcontent,	//文件内容指针
		UINT16 sendlength,	//这一次发送的长度 默认为512，注意最后一次发送的长度不一定是512
		UINT32 sendindex,//当前帧从文件内容的什么位置发送  第1次为0发送512个字节，那第2次就是512，依次类推...注意最后一次发送的长度不一定是512
		char* framebuffer)	//生成的帧内容指针 在外部malloc一块区域，注意这块区域的大小
		{
	return GetFrameData(CONTENTFILE_SET, filename, sendcontent, sendlength,
			sendindex, framebuffer);
}

void mylog(char* str) {
	return;
	FILE *fp;
	char c = '\n';

	if (str == NULL) {
		return;
	}
#ifdef WIN32
	fp = fopen("E:\\daniel\\kalerka\\log.txt","a");
#else
	fp =
			fopen(
					"//storage//sdcard0//Android//data//com.deli.kalerka//files//log.txt",
					"a");
#endif

	fprintf(fp, "%s%c", str, c);
	fclose(fp);
}

void mylog1(UINT16 str) {
	return;
	FILE *fp;
	char c = '\n';

	if (str == NULL) {
		return;
	}

#ifdef WIN32
	fp = fopen("E:\\daniel\\kalerka\\log.txt","a");
#else
	fp =
			fopen(
					"//storage//sdcard0//Android//data//com.deli.kalerka//files//log.txt",
					"a");
#endif
	fprintf(fp, "%d%c", str, c);
	fclose(fp);
}

void mylog2(char* str, int len) {
    return;
	FILE *fp;
	char c = '\n';

	if (str == NULL) {
		return;
	}

#ifdef WIN32
	fp = fopen("E:\\daniel\\kalerka\\log.txt","a");
#else
	fp =
			fopen(
					"//storage//sdcard0//Android//data//com.deli.kalerka//files//log.txt",
					"a");
#endif

	for (int index = 0; index < len; index++) {
		fprintf(fp, "%X", str[index]);
		fprintf(fp, "%c", ' ');
	}
	fprintf(fp, "%c", c);
	fclose(fp);
}

void writeContent(char* filePath, char* content, int size) {
	FILE *fp;
	//mylog(filePath);
	if (content == NULL) {
		return;
	}

#ifdef WIN32
	fp = fopen("E:\\daniel\\kalerka\\log.txt","a");
#else
	fp =fopen(filePath, "a");
#endif
	//mylog("size:");
//mylog1(size);
	for (int index = 0; index < size; index++) {
	    fprintf(fp, "%X", content[index]);
		//fwrite(fp, content[index], 1);
		//fwrite(content+index, 1, 1, fp);
	   fprintf(fp, "%c", ' ');
	}

    fprintf(fp, "%c", '\n');

	fclose(fp);
}
