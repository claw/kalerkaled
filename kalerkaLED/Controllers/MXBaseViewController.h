//
//  MXBaseViewController.h
//  kalerkaLED
//
//  Created by Calvin Law on 5/18/15.
//  Copyright (c) 2015 Calvin Law. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MXBaseViewController : UIViewController

@end
