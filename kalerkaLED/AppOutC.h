// AppOutC.h: interface for the CAppOutC class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_APPOUTC_H__8ACD85DF_B6C8_4B0D_AE41_0ACC3790555D__INCLUDED_)
#define AFX_APPOUTC_H__8ACD85DF_B6C8_4B0D_AE41_0ACC3790555D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

typedef unsigned char		byte;
typedef unsigned char		UINT8;
typedef unsigned char		INT8U;
typedef signed   char		INT8;
typedef unsigned short		UINT16;
typedef unsigned short		INT16U;
typedef signed   short		INT16;
typedef unsigned long long		UINT32;
typedef unsigned int		INT32U;
typedef signed   int		INT32;
typedef signed   int		INT32S;
typedef unsigned long		ULONG;
typedef signed   long		LONG;

typedef unsigned long       DWORD;
typedef unsigned char       BYTE;
typedef unsigned short      WORD;
typedef unsigned int		u_int;
#define NULL    0


typedef struct __file_header_dcb__
{
	char signature[3];	//文件标识字符
	UINT8 version;		//版本号
	UINT32 length;		//文件长度 不包括文件头和保留字节的长度
}FILEHEADERDCB;

#define LEDPARADCB_WRAPUPTODOWN			(1ul<<15)/*从上到下折行*/
#define LEDPARADCB_MIRROR				(1ul<<14)/*数据镜像*/
#define LEDPARADCB_SPLIT				(1ul<<13)/*二次绕线*/
#define LEDPARADCB_DATAPOLARITY			(1ul<<12)/*数据反向*/
#define LEDPARADCB_STRPOLARITY			(1ul<<11)/*STR反向*/
#define LEDPARADCB_SENDTOSERVER			(1ul<<10)/*上报位置*/
#define LEDPARADCB_AOTODRIVERBOARD		(1ul<<9)/*自动识别*/
#define LEDPARADCB_UPDATEIP				(1ul<<8)/*IP地址，子网掩码，网关，端口号是否更新*/
#define LEDPARADCB_UPDATESERVE1			(1ul<<7)/*服务器IP1和服务器端口号1是否更新*/
#define LEDPARADCB_UPDATESERVE2			(1ul<<6)/*服务器IP2和服务器端口号2是否更新*/
#define LEDPARADCB_MIRRORALTERNATE		(1ul<<5)/*镜像和正常交替*/
#define LEDPARADCB_USESEF				(1ul<<4)/*是否用描点参数*/
typedef struct __led_para_dcb__
{
	UINT16 reserve;
	UINT8 name[16];			// 名称
	INT16 width;			// 宽度
	INT16 height;			// 高度
	INT8 linesperinterface;// 每区行数 16
	INT8 lines;			// 扫描方式 16
	INT8 wrapbits;			// 绕线位数 10
	INT8 lineverify;		// 行调整 0
	UINT8 gray_basecolor;		// 基色数目(低3bit为基色数目，高5bit为灰度页面数量) 2
	INT8 frequency;		// 扫描频率www 3
	INT8 lineblanking;		// 行消隐 20
	INT8 linemode;			// 译码方式
	UINT16 bmask;			// 位控制
	UINT8 d_ipaddr[6];		// IP地址
	UINT8 d_submask[6];		// 子网掩码
	UINT8 d_gateway[6];		// 网关
	UINT16 d_port;			// 端口号
	UINT8 d_serveripaddr1[6];	// 服务器IP1
	UINT16 d_serverport1;		// 服务器端口号1
	UINT8 d_serveripaddr2[6];	// 服务器IP2
	UINT16 d_serverport2;		// 服务器端口号2
	UINT16 typeindex;		// 常用扫描方式索引
}LEDPARADCB;

typedef struct __led_file_dcb__
{
	FILEHEADERDCB fileheaderdcb;
	LEDPARADCB ledparadcb;
}LEDFILEDCB;

#define LEDPARABLK_LINEMODE				(1ul<<15)/*是否为直译*/
#define LEDPARABLK_OEPOLARITY			(1ul<<14)/*OE反向*/
#define LEDPARABLK_DATAPOLARITY			(1ul<<12)/*数据反向*/
#define LEDPARABLK_STRPOLARITY			(1ul<<11)/*STR反向*/
#define LEDPARABLK_EMPTYDOT				(1ul<<10)/*有抽点*/
#define DOTINDEX_MAX					256
#define DOTINDEX_MAX_SHIFT				8

typedef struct __dot_index__
{
	UINT8 line;		//每区行数
	UINT8 index;	//绕线次数
}DOTINDEX;

typedef struct __led_para_blk__
{
	UINT16 reserve;
	DOTINDEX dotindex[DOTINDEX_MAX];		//像素点映射
	UINT8 lineverify[16];		//行映射
	UINT16 bmask;				// 位控制
	UINT16 typeindex;			// 常用扫描方式索引  这个值传送给这个参数的时候一定要和LEDPARADCB的参数一致
	INT8 linesperinterface;	//每区行数
	UINT8 Ztimes;				//绕线次数
	UINT8 gama;					//
	UINT8 module_width;			//
	UINT8 module_empty;		//
}LEDPARABLK;

typedef struct __led_file_blk__
{
	FILEHEADERDCB fileheaderdcb;
	// 	UINT16 reserve;
	LEDPARABLK ledparablk;
}LEDFILEBLK;

typedef struct __data_time_dcb__//时间日期的结构体变量
{
	UINT16 year;			//年
	INT8 month;				//月  (1-12)
	INT8 day;				//日  (0-31)
	UINT8 week;				//星期(0-6)
	INT8 hour;				//小时(0-23)
	INT8 min;				//分钟(0-59)
	INT8 sec;				//秒  (0-59)
}DATETIMEDCB;

typedef struct __time_dcb__//时间日期的结构体变量
{						
	DATETIMEDCB begin_datatime;		//开始日期时间 其中的week是掩码
	DATETIMEDCB end_datatime;		//结束日期时间 其中的week是掩码
}TIMEDCB;

typedef union __delay_dcb__
{
	TIMEDCB timedcb;		//定时/定期播放时 日期时间参数
	INT32 timelong;		//定长播放时 时长
}DELAYDCB;

#define PROGGRAM_DELAYMODE_DELAY		0
#define PROGGRAM_DELAYMODE_DATETIME		1
#define PROGGRAM_DELAYMODE_EXTCTRL		2/*外控节目*/
#define PROGGRAM_DELAYMODE_CYCLE		3
#define PROGGRAM_DELAYMODE_ALWAYS		4
#define PROGGRAM_DELAYMODE_DMASK		(1ul<<15)
#define PROGGRAM_DELAYMODE_TMASK		(1ul<<14)
#define PROGGRAM_DELAYMODE_DTMASK		(PROGGRAM_DELAYMODE_DMASK |PROGGRAM_DELAYMODE_TMASK)
#define PROGGRAM_DELAYMODE_OKMASK		(1ul<<13)
typedef struct __program_delay_dcb__
{
	DELAYDCB delaydcb;
	UINT16 delaymode;	//低8位:0- 定长 1- 定时定期 2- 外控节目  3- 顺序播放 4- 一直播放(only1个节目的时候)bit15 日期需要比较  bit14 时间需要比较
	UINT16 reserved;
}PROGRAMDELAYDCB;

typedef struct __program_para_dcb__
{
	PROGRAMDELAYDCB programdelaydcb;//节目播放时长参数
	char programfilename[16];		//节目内容文件名
	char backgroudfilename[16];		//背景文件名
}PROGRAMPARADCB;

typedef struct __content_para_dcb__
{
	UINT8 reserve[2];
	INT16 programnum;
}CONTENTPARADCB;

typedef struct __content_file_dcb__
{
	FILEHEADERDCB fileheaderdcb;
	CONTENTPARADCB contentparadcb;
}CONTENTFILEDCB;

typedef struct __water_display_dcb__
{//与文件中的参数对应
	UINT32 watercontentoffset;	//流水边框图片在此文件中相对此位置地址偏移量
	INT8 width;					//一幅图片的上边和下边的宽度(即左边和右边的高度)最好以16的整数倍为一个循环单位且至少为height的偶数倍
	INT8 height;				//一幅图片的上边和下边的高度，左边和有点的宽度
	UINT8 speed;				//移动速度
	UINT8 mode;					//bit7- 是否显示流水边框
	//bit6-bit5 流水边框的显示模式00-逆时针 10-顺时针 01-静态   bit0- 是否闪烁 0-不闪烁 1- 闪烁
}WATERDISPLAYDCB;

typedef struct __program_file_dcb__
{
	FILEHEADERDCB fileheaderdcb;
	UINT8 reserve[2];
	UINT16 areanum;
	WATERDISPLAYDCB waterdisplaydcb;
}PROGRAMFILEDCB;

typedef struct __show_para_dcb__				
{				
	UINT32 contentoffset;		//流水边框图片在此文件中地址偏移量
	INT8 entermode;		//显示方式
	INT8 exitmode;			//显示方式
	// 	UINT8 entermode;		//显示方式
	// 	UINT8 exitmode;			//显示方式
	UINT8 speed;			//上屏速度
	UINT8 delay;			//停留时间 单位和定时器的最小定时单位一致
	UINT8 begincur;			//使用的小图标的起始标号
	UINT8 endcur;			//使用的小图标的结束标号 这两个参数只在特定的上屏方式中使用
	UINT8 delayH;
	UINT8 shiftbit;			//为了保证结构体是4个整数倍个字节
}SHOWPARADCB;

typedef struct __showex_para_dcb__				
{				
	INT16 width;		//流水边框图片在此文件中地址偏移量
	INT16 height;		//显示方式
	INT16 reserved1;			//显示方式
	INT16 reserved2;			//上屏速度
}SHOWEXPARADCB;

typedef struct __rectangular_para_dcb__
{
	INT16 x;			//相对于显示屏x坐标
	INT16 y;			//相对于显示屏y坐标
	INT16 width;		//宽度
	INT16 height;		//高度
}RECTANGULARPARADCB;

#define AREA_TYPE_PICTURE		1/*图片*/
#define AREA_TYPE_DATETIME		2/*数字时钟*/
#define AREA_TYPE_WATCH			3/*模拟时钟*/
#define AREA_TYPE_COUNTER		4/*计时*/
#define AREA_TYPE_LUNAR			5/*农历*/
#define AREA_TYPE_DANCE			7/*炫动字*/
#define AREA_TYPE_TEMPRETURE	8/*温度*/
#define AREA_TYPE_HUMIDITY		9/*湿度*/
#define AREA_TYPE_TEXT			10/*文本*/

typedef struct __picture_para_dcb__
{
	UINT8 reserve1[2];
	UINT16 areatype;
	RECTANGULARPARADCB rectangularparadcb;
	UINT32 picparaaddr;
	WATERDISPLAYDCB waterdisplaydcb;
	UINT16 pictotol;
	UINT16 icototol;
	UINT32 icoparaaddr;
	UINT32 reserve2;
}PICTUREPARADCB;

typedef struct __mybitmapfileheader__
{ 
	WORD    bfType; 
	DWORD   bfSize; 
	WORD    bfReserved1; 
	WORD    bfReserved2; 
	DWORD   bfOffBits; 
}myBITMAPFILEHEADER;

typedef struct __mybitmapinfoheader__
{ 
	DWORD      biSize; 
	LONG        biWidth; 
	LONG        biHeight; 
	WORD       biPlanes; 
	WORD       biBitCount; 
	DWORD      biCompression; 
	DWORD      biSizeImage; 
	LONG        biXPelsPerMeter; 
	LONG        biYPelsPerMeter; 
	DWORD      biClrUsed; 
	DWORD      biClrImportant; 
}myBITMAPINFOHEADER;

typedef enum __getpicture_result__
{
	GETPICTURE_SUCCESS =0,
		GETPICTURE_NOFILE,//		1
		GETPICTURE_NOTBMP,//		2
		GETPICTURE_NOT24OR32BITS//		3
}GETPICTURE_RESULT;

typedef struct __myrgb32__
{
	BYTE    rgbreserve;
	BYTE    rgbBlue;
	BYTE    rgbGreen;
	BYTE    rgbRed;
} myRGB32;

typedef struct __myrgb24__
{
	BYTE    rgbBlue;
	BYTE    rgbGreen;
	BYTE    rgbRed;
} myRGB24;

typedef struct __picture_file_dcb__
{
	FILEHEADERDCB fileheaderdcb;
	PICTUREPARADCB pictureparadcb;
}PICTUREFILEDCB;

#define RS232_RECEIVE_HEAD_POS			0	//帧头位置
#define RS232_RECEIVE_HEAD_SUM			3	//帧头数量
#define RS232_RECEIVE_DESADDR_POS		(RS232_RECEIVE_HEAD_POS +RS232_RECEIVE_HEAD_SUM)	//目的地址位置
#define RS232_RECEIVE_DESADDR_SUM		4	//目的地址数量
#define RS232_RECEIVE_SRCADDR_POS		(RS232_RECEIVE_DESADDR_POS +RS232_RECEIVE_DESADDR_SUM)	//源地址位置
#define RS232_RECEIVE_SRCADDR_SUM		4	//源地址数量
#define RS232_RECEIVE_DATATYPE_POS		(RS232_RECEIVE_SRCADDR_POS +RS232_RECEIVE_SRCADDR_SUM)	//数据类型位置
#define RS232_RECEIVE_DATATYPE_SUM		4	//数据类型数量
#define RS232_RECEIVE_DATALENGHT_POS	(RS232_RECEIVE_DATATYPE_POS +RS232_RECEIVE_DATATYPE_SUM)	//数据长度位置
#define RS232_RECEIVE_DATALENGHT_SUM	4	//数据长度数量
#define RS232_RECEIVE_DATA_POS			(RS232_RECEIVE_DATALENGHT_POS +RS232_RECEIVE_DATALENGHT_SUM)	//数据内容位置
#define RS232_RECEIVE_VERIFY_SUM		2	//校验数量
#define RS232_RECEIVE_TRAIL_SUM			3	//帧尾数量
const UINT8 RS232_HEAD[RS232_RECEIVE_HEAD_SUM] ={'K', 'L', 'B'};
const UINT8 RS232_TRAIL[RS232_RECEIVE_TRAIL_SUM] ={'K', 'L', 'E'};

#define PARAFILE_SET					0/*显示屏显示的参数设置和读取*/
#define PARAFILE_READ					1/*保存显示屏显示的参数*/
#define CONTENTFILE_SET					4/*显示屏显示的参数设置和读取*/
#define CONTENTFILE_READ				5/*保存显示屏显示的参数*/
#define PARAEXTFILE_SET					10	/*显示屏扩展参数设置*/
#define PARAEXTFILE_READ				11	/*显示屏扩展参数设置*/

class CAppOutC  
{
public:
	CAppOutC();
	virtual ~CAppOutC();

	UINT16 GetScfFrameData(char* sendcontent,//文件内容指针
		char* framebuffer);//生成的帧内容指针 在外部malloc一块区域，注意这块区域的大小
	
	UINT16 GetSefFrameData(char* sendcontent,//文件内容指针
		char* framebuffer);//生成的帧内容指针 在外部malloc一块区域，注意这块区域的大小
	
	UINT16 GetContentFrameData(char* filename,//ccf pcf和acf文件的真是文件名 一个工程里面不能有相同的文件名，需要软件做内容的格式命名
		char* sendcontent,//文件内容指针
		UINT16 sendlength,//这一次发送的长度 默认为512，注意最后一次发送的长度不一定是512
		UINT32 sendindex,//当前帧从文件内容的什么位置发送  第1次为0发送512个字节，那第2次就是512，依次类推...注意最后一次发送的长度不一定是512
		char* framebuffer);//生成的帧内容指针 在外部malloc一块区域，注意这块区域的大小
	// modify by daniel start
	/*UINT16 CAppOutC::GetFrameData(UINT16 frametype,
		char* filename,
		char* sendcontent,
		UINT16 sendlength,
		UINT32 sendindex,
		char* framebuffer); */
	UINT16 GetFrameData(UINT16 frametype,
		char* filename,
		char* sendcontent,
		UINT16 sendlength,
		UINT32 sendindex,
		char* framebuffer);
	// modify by daniel end
	GETPICTURE_RESULT GetWaterSize(char *bmpfilename/*��·��*/, UINT8 basecolor, UINT32* picsizeH, UINT32* picsizeV);
	GETPICTURE_RESULT GetPictureSize(char *bmpfilename, UINT8 basecolor, UINT32* picsize);
	GETPICTURE_RESULT ConvertPictureContentX(char *filecontent/*包含路径*/, UINT8 basecolor, UINT16* bmpcontent, UINT8 bigendian);
	GETPICTURE_RESULT ConvertPictureContent(char *bmpfilename/*包含路径*/, UINT8 basecolor, UINT16* bmpcontent, UINT8 bigendian);
	bool GetScfSef(char *screenname, char *scfbuffer, char *sefbuffer);
	bool CreateScfSef(LEDPARADCB *ledparadcb, LEDPARABLK *ledparablk, char *scfbuffer, char *sefbuffer);
	bool CreateCcf(short programnum, PROGRAMPARADCB* programparadcb, char* ccfbuffer);
	bool CreatePcf(UINT16 areanum, char* areafilename, WATERDISPLAYDCB* waterdisplaydcb,
		UINT32* watercontent, char* pcfbuffer, UINT8 basecolor);
	bool CreateAcfPicture(PICTUREPARADCB* pictureparadcb, SHOWEXPARADCB* showexparadcb, SHOWPARADCB* showparadcb,
		UINT32* piccontent,
		UINT32* watercontent, char* acfbuffer, UINT8 basecolor);


	UINT32 GetCcfLength(short programnum);//2015-02-26新增加
	UINT32 GetAcfPictureLength(PICTUREPARADCB* pictureparadcb, UINT8 basecolor);//2015-02-26新增加
	UINT32 GetPcfLength(UINT16 areanum,
						  WATERDISPLAYDCB* waterdisplaydcb,
						  UINT8 basecolor);
};
void mylog(char* str);
void mylog1(UINT16 str);
void mylog2(char* str, int len);
void writeContent(char* filePath, char* content, int size);
#endif // !defined(AFX_APPOUTC_H__8ACD85DF_B6C8_4B0D_AE41_0ACC3790555D__INCLUDED_)
